FROM ubuntu:18.04
RUN apt-get update
RUN apt-get install -y python3 python3-pip
RUN pip3 install pandas==1.0.5 unidecode==1.1.1 matplotlib==3.1.3 requests==2.24.0 Flask==1.0.2 python-dotenv
ADD src /webapp/src
WORKDIR /webapp/src
ENTRYPOINT python3 app.py
